#ifndef __CONFIG_SEMANTIC_LABELING_HEADER
#define __CONFIG_SEMANTIC_LABELING_HEADER

#include <cstring>
#include <string>
using std::string;

#include <vector>
using std::vector;

//#include <utility>
//using std::pair;
#include <fstream>
using std::ifstream;

#include "opencv2/opencv.hpp"

class Config_Module
{
public:
	Config_Module();	
	Config_Module(string cf);
	~Config_Module(){};

	int Init();	
	int ReadConfigFile(string cfn);
	int SetDefaultConfig();
	int WriteConfigFile(string cfn);

	string configFileName;

	//string trainImgFolder;
	string trainImgList;
	vector<string> trainImgFileFolderName;
	vector<string> trainImgFileName;

	//string trainLabelFolder;
	string trainLabelList;
	vector<string> trainLabelFileName;


	//string testImgFolder;
	string testImgList;
	vector<string> testImgFileFolderName;
	vector<string> testImgFileName;

	bool haveTestLabel;
	string testLabelList;
	vector<string> testLabelFileName;

	string labelFile;
	string labelOutFileSuffix;
	string labelColorCodeFile;
	vector<string> labelColorName;
	vector<vector<cv::Vec3b>> labelColor;

	string featureFolder;
	string superpixelFile;
	string extractFeatureFile;
	vector<string> extractFeatureList;

	// declare additional operation here
	int ReadList(string &listName, vector<string> &listStrings);
	int ReadLabelColorFile();


	// learning program
	string libSVMTrainExe;
	string libSVMPredictExe;
	string libSVMScaleExe;
	string libSVMModelFile;
	string libSVMScaledRange;

	string libSVMTrainPara;
	string libSVMTestPara;

	bool visualizeOriginalLabel;

	bool CLEAN_EXIST_DATA_FLAG;

	cv::Mat_<float> superpixelParameters;
};


#endif