#include "Config_Semantic_Labeling_Header.h"

int Config_Module::Init()
{
	featureFolder = string("features\\");
	superpixelFile = string("superpixel.txt");
	labelFile = string("label.txt");
	labelOutFileSuffix = string(".label.out.txt");
	return 0;
}

Config_Module::Config_Module()
{
	Init();
}

Config_Module::Config_Module( string cf )
{
	Init();

	configFileName = cf;
	ReadConfigFile(configFileName);

	// read training image and label 
	ReadList(trainImgList, trainImgFileName);
	trainImgFileFolderName.clear();
	for (int iy=0; iy<trainImgFileName.size(); ++iy)
	{
		trainImgFileFolderName.push_back(trainImgFileName[iy].substr(0, trainImgFileName[iy].find_last_of('.')));
	}
	ReadList(trainLabelList, trainLabelFileName);

	// read test image and label
	ReadList(testImgList, testImgFileName);
	testImgFileFolderName.clear();
	for (int iy=0; iy<testImgFileName.size(); ++iy)
	{
		testImgFileFolderName.push_back(testImgFileName[iy].substr(0, testImgFileName[iy].find_last_of('.')));
	}

	if (haveTestLabel) ReadList(testLabelList, testLabelFileName);

	ReadList(extractFeatureFile, extractFeatureList);

	ReadLabelColorFile();
}

int Config_Module::WriteConfigFile( string cfn )
{
	cv::FileStorage fs(cfn, cv::FileStorage::WRITE);

	//fs << "TrainImgFolder" << trainImgFolder;
	fs << "TrainImgList" << trainImgList;

	//fs << "TrainLabelFolder" << trainLabelFolder;
	fs << "TrainLabelList" << trainLabelList;

	fs << "LabelColorCodingFile" << labelColorCodeFile;
	fs << "ExtractFeature" << extractFeatureFile;

	//fs << "TestImgFolder" << testImgFolder;
	fs << "TestImgList" << testImgList;
	fs << "TestLabelList" << testLabelList;
	fs << "HaveTestLabel" << haveTestLabel;
	fs.release();
	return 0;
}

int Config_Module::SetDefaultConfig()
{
	//trainImgFolder.assign("train_img");
	trainImgList.assign("train_img_list.txt");

	//trainLabelFolder.assign("train_label");
	trainLabelList.assign("train_label_list.txt");

	labelColorCodeFile.assign("label_color_coding.txt");
	extractFeatureFile.assign("extract_feature.txt");

	//testImgFolder.assign("test_img");
	testImgList.assign("test_img_list.txt");
	testLabelList.assign("");
	haveTestLabel = false;

	return 0;
}

int Config_Module::ReadConfigFile( string cfn )
{
	cv::FileStorage fs(cfn, cv::FileStorage::READ);

	//fs["TrainImgFolder"] >> trainImgFolder;
	fs["TrainImgList"] >> trainImgList;

	//fs["TrainLabelFolder"] >> trainLabelFolder;
	fs["TrainLabelList"] >> trainLabelList;

	fs["LabelColorCodingFile"] >> labelColorCodeFile;
	fs["ExtractFeature"] >> extractFeatureFile;

	//fs["TestImgFolder"] >> testImgFolder;
	fs["TestImgList"] >> testImgList;
	fs["TestLabelList"] >> testLabelList;

	fs["HaveTestLabel"] >> haveTestLabel;

	// learning related
	fs["libSVMTrain"] >> libSVMTrainExe;
	fs["libSVMPredict"] >> libSVMPredictExe;
	fs["libSVMScale"] >> libSVMScaleExe;
	fs["libSVMModelFile"] >> libSVMModelFile;
	fs["libSVMScaledRange"] >> libSVMScaledRange;

	fs["libSVMTrainParameters"] >> libSVMTrainPara;
	fs["libSVMTestParameters"] >> libSVMTestPara;

	fs["VisualizeOriginalLabels"] >> visualizeOriginalLabel;

	fs["CleanExistDataFlag"] >> CLEAN_EXIST_DATA_FLAG;

	fs["SuperpixelParameters"] >> superpixelParameters;
	fs.release();
	return 0;
}

int Config_Module::ReadList( string &listName, vector<string> &listStrings )
{
	if (listName.empty()) return 0;

	listStrings.clear();
	// ifstream fin(listName, ifstream::in);
	FILE *fin = fopen(listName.c_str(), "r");
	// printf("%s\n", listName.c_str());
	assert(fin != NULL);

	char tmpStr[256];

	// while (fin.getline(tmpStr, 256))
	while (fgets(tmpStr, 256, fin) != NULL)
	{
		// change \n to \0
		if (tmpStr[strlen(tmpStr)-1] == '\n') tmpStr[strlen(tmpStr)-1] = '\0';
		// skip empty line or space line or tab
		if (string(tmpStr).find_first_not_of(" \t") == string::npos) continue;
		listStrings.push_back(tmpStr);
	}

	// fin.close();
	fclose(fin);
	return 0;
}

int Config_Module::ReadLabelColorFile()
{
	vector<string> labelColorLines;
	ReadList(labelColorCodeFile, labelColorLines);
	
	labelColorName.clear();
	labelColor.clear();

	for (int i=0; i<labelColorLines.size(); ++i)
	{
		if (labelColorLines[i].empty()) continue;
		char tmpStr[256];
		int tmpC[3];
		sscanf(labelColorLines[i].c_str(), "%s %d %d %d", tmpStr, &tmpC[0], &tmpC[1], &tmpC[2]);
		int k;
		for (k=0; k<labelColorName.size(); ++k)
		{
			if (labelColorName[k] == string(tmpStr))
				break;
		}
		if (k >= labelColorName.size()) 
		{
			labelColorName.push_back(tmpStr);
			// should be in BGR format
			labelColor.push_back(vector<cv::Vec3b>(1, cv::Vec3b(tmpC[0], tmpC[1], tmpC[2])));
		}
		else
		{
			labelColor[k].push_back(cv::Vec3b(tmpC[0], tmpC[1], tmpC[2]));
		}
	}
	return labelColorLines.size();
}

