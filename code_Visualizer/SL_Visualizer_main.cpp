#include "../Superpixel_Module/Superpixel_Header.h"
#include "../Config_Module/Config_Semantic_Labeling_Header.h"
#include "../Utility_File/Util_Directory_File.h"
#include "opencv2/opencv.hpp"

#include <iostream>
using namespace std;

#include "../Utility_File/Util_DataPreparation.h"

void VisualizeFiles(Config_Module &tcon, vector<string> &imgFileName, vector<string> &imgFileFolderName, int trainOrTest);
const string TRAIN_LABEL_COLOR_FILE("label_color.png");
const string TEST_LABEL_COLOR_FILE("label_out_color.png");

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		printf("Lacking of task config file!\nPlease use relative path!\n");
		exit(-1);
	}

	char curDir[256];
	strcpy(curDir, argv[1]);
	while (curDir[strlen(curDir)-1] != '\\')
	{
		curDir[strlen(curDir)-1] = '\0';
	}
	printf("%s %d\n", curDir, chdir(curDir));
	int retVal = chdir(curDir);
	assert(0 == retVal);
	// read task configuration file
	string taskConfig(argv[1]);
	Config_Module tcon(taskConfig);

	printf("FINISH: read config file %s\n\n", argv[1]);
	char tmpCwd[256];
	getcwd(tmpCwd, 256);
	printf("Current working directory: %s\n\n", tmpCwd);

	int iy;
	// visualize test label out
	VisualizeFiles(tcon, tcon.testImgFileName, tcon.testImgFileFolderName, 2);
	printf("FINISH: visualizing test output labels\n\n");

	// visualze train and test original label
	if (tcon.visualizeOriginalLabel)
	{
		VisualizeFiles(tcon, tcon.testImgFileName, tcon.testImgFileFolderName, 1);
		printf("FINISH: visualizing test original labels\n\n");
		VisualizeFiles(tcon, tcon.trainImgFileName, tcon.trainImgFileFolderName, 1);
		printf("FINISH: visualizing train original labels\n\n");
	}

	return 0;
}

void VisualizeImgSegLabel(const cv::Mat_<cv::Vec3b> &img, const cv::Mat_<int> &seg, vector<int> &label, vector<vector<cv::Vec3b>> &labelColor, string folder, string labelColorFile) 
{
	cv::Mat_<cv::Vec3b> showImg;
	Superpixel_Module::DrawContoursAroundSegments(img, seg, showImg);
	cv::imwrite(FullFile(folder, "img_seg.jpg"), showImg);

	cv::Mat_<cv::Vec3b> labelImg(img.size());
	labelImg.setTo(cv::Vec3b(255, 255, 255));
	int iy, ix;
	int height = img.rows;
	int width = img.cols;
	for (iy=0; iy<height; ++iy)
	{
		for (ix=0; ix<width; ++ix)
		{
			int tmpLabel = label[seg[iy][ix]]-1;
			if (tmpLabel >= 0) labelImg[iy][ix] = labelColor[tmpLabel][0];
		}
	}
	
	cv::imwrite(FullFile(folder, labelColorFile), labelImg);
}

void VisualizeFiles(Config_Module &tcon, vector<string> &imgFileName, vector<string> &imgFileFolderName, int trainOrTest)
{
	const bool CLEAN_DATA_FLAG = tcon.CLEAN_EXIST_DATA_FLAG;

	string labelFile;
	string outputLabelColorFile;
	if (trainOrTest == 1)
	{
		// train
		labelFile = tcon.labelFile;
		outputLabelColorFile = TRAIN_LABEL_COLOR_FILE;
	}
	else if (trainOrTest == 2)
	{
		// test
		labelFile = FullFile(DATA_FILE_NAME, tcon.labelOutFileSuffix, 1);
		outputLabelColorFile = TEST_LABEL_COLOR_FILE;
	}

	int iy, ix;
	for (iy=0; iy<imgFileName.size(); ++iy)
	{
		string feaImgDir = FullFile(tcon.featureFolder, imgFileFolderName[iy]);
		
		if (CLEAN_DATA_FLAG) remove(FullFile(feaImgDir, outputLabelColorFile));
		if (ExistFile(FullFile(feaImgDir, outputLabelColorFile)))
		{
			cout << feaImgDir << " exist colored label file!! Skipped" << endl;
			continue;
		}
		cout << feaImgDir << endl;

		string segFile = FullFile(feaImgDir, tcon.superpixelFile);
		cv::Mat_<int> seg;
		int numOfSeg;
		Superpixel_Module::ReadInSegFromFile(segFile, seg, numOfSeg);

		cv::Mat_<cv::Vec3b> img = cv::imread(imgFileName[iy]);
		vector<int> label(numOfSeg, 0);

		FILE *fin = fopen(FullFile(feaImgDir, labelFile).c_str(), "r");
		if (trainOrTest == 1)
		{
			// if train file, skip the first line, two integers
			int tmp;
			fscanf(fin, "%d %d", &tmp, &tmp);
		}
		for (ix=0; ix<numOfSeg; ++ix)
		{
			fscanf(fin, "%d", &label[ix]);
		}
		fclose(fin);

		VisualizeImgSegLabel(img, seg, label, tcon.labelColor, feaImgDir, outputLabelColorFile);
	}
}
