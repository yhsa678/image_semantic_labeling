#include "../Config_Module/Config_Semantic_Labeling_Header.h"
#include "../Utility_File/Util_Directory_File.h"
#include "opencv2/opencv.hpp"

#include <iostream>
using namespace std;

#include "../Utility_File/Util_DataPreparation.h"


int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		printf("Lacking of task config file!\nPlease use relative path!\n");
		exit(-1);
	}

	char curDir[256];
	strcpy(curDir, argv[1]);
	while (curDir[strlen(curDir)-1] != '\\')
	{
		curDir[strlen(curDir)-1] = '\0';
	}
	printf("%s %d\n", curDir, chdir(curDir));
	int retVal = chdir(curDir);
	assert(0 == retVal);
	// read task configuration file
	string taskConfig(argv[1]);
	Config_Module tcon(taskConfig);

	printf("FINISH: read config file %s\n\n", argv[1]);
	char tmpCwd[256];
	getcwd(tmpCwd, 256);
	printf("Current working directory: %s\n\n", tmpCwd);

	// compile training data
	printf("START: reading and compile training data\n\n");
	string trainDataFile = FullFile(tcon.featureFolder, TRAINING_DATA_FILE_NAME);
	CompileDataFile(trainDataFile, tcon, 1);
	printf("FINISH: reading and compile training data\n");;
	printf("The training data file %s\n\n", trainDataFile.c_str());


	// compile testing data
	/*printf("START: reading and compile testing data\n\n");
	string testDataFile = FullFile(tcon.featureFolder, TESTING_DATA_FILE_NAME);
	CompileDataFile(testDataFile, tcon, 2);
	printf("FINISH: reading and compile testing data\n\n");;
	printf("The testing data file %s\n", testDataFile.c_str());*/

	// train the model on compiled training data
	// scaled data first
	string trainDataScaled(FullFile(trainDataFile, ".scale.txt", 1));
	char svmCmd[512];
	sprintf(svmCmd, "%s -l -1 -u 1 -s %s %s > %s\0", tcon.libSVMScaleExe.c_str(), tcon.libSVMScaledRange.c_str(), trainDataFile.c_str(), trainDataScaled.c_str());
	printf("%s\n\n", svmCmd);
	system(svmCmd);

	// train and save out the model
	sprintf(svmCmd, "%s %s %s %s\0", tcon.libSVMTrainExe.c_str(), tcon.libSVMTrainPara.c_str()
		, trainDataScaled.c_str(), tcon.libSVMModelFile.c_str());
	printf("%s\n\n", svmCmd);
	system(svmCmd);

	printf("FINISH: output training model %s\n", tcon.libSVMModelFile.c_str());
	return 0;
}
