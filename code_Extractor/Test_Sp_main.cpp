#include "../Superpixel_Module/Superpixel_Header.h"
#include "opencv2/opencv.hpp"
#include <iostream>
using namespace std;
int main2()
{
	cv::Mat_<cv::Vec3b> img = cv::imread("3_24_s.bmp");
	cv::Mat_<int> seg;
	int numOfSegs = Superpixel_Module::CreateSuperpixelsFromImage(img, 1, seg);
	cout << "Number = " << numOfSegs << endl;
	cv::Mat_<cv::Vec3b> showImg;
	Superpixel_Module::DrawContoursAroundSegments(img, seg, showImg);
	cv::imshow("Test", showImg);

	Superpixel_Module sm;
	sm.SetImage(img);
	float tmp[4] = {1.0, 0.5, 300, 300};
	sm.SetParameters(vector<float>(tmp, tmp+4));
	cv::Mat_<int> seg2;
	int numOfSegs2 = sm.GetSuperpixels(seg2);
	cout << "Number2 = " << numOfSegs2 << endl;
	cv::Mat_<cv::Vec3b> showImg2;
	Superpixel_Module::DrawContoursAroundSegments(img, seg2, showImg2);
	cv::imshow("Test2", showImg2);

	cv::waitKey(0);
	return 0;
}