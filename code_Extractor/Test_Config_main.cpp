#include "../Config_Module/Config_Semantic_Labeling_Header.h"

#include "opencv2/opencv.hpp"
#include <iostream>
using namespace std;
int main3()
{
	Config_Module cm;
	cm.SetDefaultConfig();
	cm.WriteConfigFile("Task1_config.xml");

	cm.ReadConfigFile("Task1_config.xml");
	cm.WriteConfigFile("testOut.xml");

	cm.ReadList(string("C:\\Hongsheng\\3_Project_Runtime_Exp\\Test_Semantic_Labeling\\Test_Files\\extract_feature.txt"), cm.extractFeatureList);
	for (int i=0; i<cm.extractFeatureList.size(); ++i)
	{
		cout << cm.extractFeatureList[i] << endl;
	}

	cm.labelColorCodeFile = "C:\\Hongsheng\\3_Project_Runtime_Exp\\Test_Semantic_Labeling\\Test_Files\\label_color_coding.txt";
	cm.ReadLabelColorFile();
	for (int i=0; i<cm.labelColorName.size(); ++i)
	{
		cout << cm.labelColorName[i] << endl;
		cout << cm.labelColor[i][0] << endl;
	}
	return 0;
}