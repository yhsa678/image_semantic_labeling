#include "../Superpixel_Module/Superpixel_Header.h"
#include "../Config_Module/Config_Semantic_Labeling_Header.h"
#include "../Utility_File/Util_Directory_File.h"
#include "opencv2/opencv.hpp"

#include <iostream>
using namespace std;

#include "../Feature_Module/Feature_Extractor_Header.h"
#include "../Utility_File/Util_DataPreparation.h"
#include "../Utility_File/OpenCV_Tickcount_Header.h"

int ExtractFeatureAndLabelFromFile(Config_Module &tcon, Superpixel_Module &sm, Feature_Extractor &fe, int trainOrTest);

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		printf("Lacking of task config file!\nPlease use relative path!\n");
		exit(-1);
	}
	
	char curDir[256];
	strcpy(curDir, argv[1]);
	while (curDir[strlen(curDir)-1] != '\\')
	{
		curDir[strlen(curDir)-1] = '\0';
	}
	//printf("%s %d\n", curDir, chdir(curDir));
	int retVal = chdir(curDir);
	//current_path(curDir);
	assert(0 == retVal);
	// read task configuration file
	string taskConfig(argv[1]);
	Config_Module tcon(taskConfig);

	printf("FINISH: read config file %s\n\n", argv[1]);
	char tmpCwd[256];
	getcwd(tmpCwd, 256);
	printf("Current working directory: %s\n\n", tmpCwd);
	//cout << current_path().string() << "\n";

	// extract superpixels
	Superpixel_Module sm;
	vector<float> tmp(tcon.superpixelParameters.rows, 0.0);
	for (int i=0; i<tcon.superpixelParameters.rows; ++i)
		tmp[i] = tcon.superpixelParameters[i][0];
	cout << tcon.superpixelParameters << endl;
	sm.SetParameters(tmp);

	Feature_Extractor fe;
	fe.InitLabelColorMapping(tcon.labelColor);

	printf("START: - Training data -\n\n");
	printf("START: extracting superpixels and other features\n\n");
	printf("START: extracting label for training data\n\n");

	ExtractFeatureAndLabelFromFile(tcon, sm, fe, 1);

	printf("FINISH: extract superpixels and features\n\n");
	printf("FINISH: extract label for training data\n\n");
	printf("FINISH: - Training data -\n\n");


	// for test data
	printf("START: - Testing data -\n\n");
	printf("START: extracting superpixels and other features\n\n");
	printf("START: extracting label for testing data\n\n");
	
	ExtractFeatureAndLabelFromFile(tcon, sm, fe, 2);

	printf("FINISH: extract superpixels and features\n\n");
	printf("FINISH: extract label for testing data\n\n");
	printf("FINISH: - Testing data -\n\n");
	return 0;
}

int ExtractFeatureAndLabelFromFile(Config_Module &tcon, Superpixel_Module &sm, Feature_Extractor &fe, int trainOrTest)
{
	const bool CLEAN_DATA_FLAG = tcon.CLEAN_EXIST_DATA_FLAG;

	vector<string> imgFileName;
	vector<string> imgFileFolderName;
	vector<string> labelFileName;
	bool haveLabel = true;
	if (trainOrTest == 1)
	{
		// train
		imgFileName = tcon.trainImgFileName;
		imgFileFolderName = tcon.trainImgFileFolderName;
		labelFileName = tcon.trainLabelFileName;
		haveLabel = true;
	}
	else if (trainOrTest == 2)
	{
		// test
		imgFileName = tcon.testImgFileName;
		imgFileFolderName = tcon.testImgFileFolderName;
		labelFileName = tcon.testLabelFileName;
		haveLabel = tcon.haveTestLabel;
	}
	else return -1;

	mkdir(tcon.featureFolder.c_str());
	int iy, ix;
	for (int iy=0; iy<imgFileName.size(); ++iy)
	{
		string feaImgDir = FullFile(tcon.featureFolder, imgFileFolderName[iy]);
		
		// make directory that may have more than one level, makedir() would fail in this case
		MakeDeepDir(feaImgDir);

		// clean directory if needed
		if (CLEAN_DATA_FLAG) remove_all(feaImgDir);

		// if exist extracted data, walk away
		int retVal = mkdir(feaImgDir.c_str());
		if (ExistFile(FullFile(feaImgDir, DATA_FILE_NAME)))
		{
			cout << feaImgDir << " existed!!" << endl;
			continue;
		}
		cout << feaImgDir << endl;

		cv::Mat_<int> seg;
		cv::Mat_<cv::Vec3b> img = cv::imread(imgFileName[iy]);
		sm.SetImage(img);
		int numOfSegs = sm.GetSuperpixels(seg);
		sm.WriteOutSegToFile(seg, numOfSegs, FullFile(feaImgDir, tcon.superpixelFile));

		CalcTime ct;
		ct.Start();
		fe.SetDataForExtractingFeature(img, seg, numOfSegs, feaImgDir);
		fe.ExtractFeatures(tcon.extractFeatureList);
		ct.End("Extract features from one image costs");

		cv::Mat_<cv::Vec3b> labelImg;
		if (haveLabel) labelImg = cv::imread(labelFileName[iy]);
		else
		{
			labelImg.create(img.size());
			labelImg.setTo(cv::Vec3b(0, 0, 0));
		}

		fe.ExtractLabel(labelImg, feaImgDir, tcon.labelFile);
		WriteFeatureAndLabelIntoLibSVMForm(feaImgDir, tcon.extractFeatureList, tcon.labelFile, DATA_FILE_NAME);
	}
}