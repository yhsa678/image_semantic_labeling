#include "Util_DataPreparation.h"

int WriteFeatureAndLabelIntoLibSVMForm(string dataFolder, vector<string> &feaList, string labelFile, string dataFileName)
{
	FILE *fout = fopen(FullFile(dataFolder, dataFileName).c_str(), "w");
	int numDim = feaList.size()+1;
	vector<FILE *> fin(numDim, NULL);

	fin[0] = fopen(FullFile(dataFolder, labelFile).c_str(), "r");
	assert(fin[0] != NULL);
	int iy, ix;
	for (iy=1; iy<numDim; ++iy)
	{
		fin[iy] = fopen(FullFile(dataFolder, FullFile(feaList[iy-1], ".txt", 1)).c_str(), "r");
		assert(fin[iy] != NULL);
	}

	int numSp;
	vector<int> numRowFea(numDim, 0);

	for (iy=0; iy<numDim; ++iy)
	{
		fscanf(fin[iy], "%d %d", &numSp, &numRowFea[iy]);
	}

	for (iy=0; iy<numSp; ++iy)
	{
		int tmpLabel;
		fscanf(fin[0], "%d", &tmpLabel);
		fprintf(fout, "%d ", tmpLabel);
		int idDim = 1;
		for (ix=1; ix<numDim; ++ix)
		{
			int k;
			for (k=0; k<numRowFea[ix]; ++k)
			{
				float tmp;
				fscanf(fin[ix], "%f", &tmp);
				fprintf(fout, "%d:%f ", idDim++, tmp);
			}
		}
		fprintf(fout, "\n");
	}

	for (iy=0; iy<numDim; ++iy)
	{
		fclose(fin[iy]);
	}

	fclose(fout);
	return 0;
}


void CompileDataFile(const string dataFile, Config_Module &tcon, int trainOrTest)
{
	const bool CLEAN_DATA_FLAG = tcon.CLEAN_EXIST_DATA_FLAG;

	vector<string> imgFileName;
	vector<string> imgFileFolderName;
	if (trainOrTest == 1)
	{
		// train
		imgFileName = tcon.trainImgFileName;
		imgFileFolderName = tcon.trainImgFileFolderName;
	}
	else if (trainOrTest == 2)
	{
		// test
		imgFileName = tcon.testImgFileName;
		imgFileFolderName = tcon.testImgFileFolderName;
	}

	if (ExistFile(dataFile)) 
	{
		if (CLEAN_DATA_FLAG) remove_all(dataFile);
		else printf("The data file existed. \n Append data to: \n%s\n", dataFile.c_str());
	}

	for (int iy=0; iy<imgFileName.size(); ++iy)
	{
		string feaImgDir = FullFile(tcon.featureFolder, imgFileFolderName[iy]);
		cout << feaImgDir << endl;

		// clean directory if needed
		// rmdir(feaImgDir.c_str());
		string tmpFile = FullFile(feaImgDir, DATA_FILE_NAME);
		if (!ExistFile(tmpFile.c_str()))
		{
			printf("%s does not existed!!! - Skipped!\n", tmpFile.c_str());
			continue;
		}

		CatTwoFiles(dataFile, tmpFile);		
	}
}
