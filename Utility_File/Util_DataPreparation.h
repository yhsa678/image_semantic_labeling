#ifndef __UTILITY_DATA_PREPARATION_HEADER
#define __UTILITY_DATA_PREPARATION_HEADER

#include "Util_Directory_File.h"
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <iostream>
using std::cout;
using std::endl;

#include "../Config_Module/Config_Semantic_Labeling_Header.h"

const string DATA_FILE_NAME("libSVM_dat.txt");
const string TRAINING_DATA_FILE_NAME("libSVM_dat_train.txt");
const string TESTING_DATA_FILE_NAME("libSVM_dat_test.txt");

int WriteFeatureAndLabelIntoLibSVMForm(string dataFolder, vector<string> &feaList, string labelFile, string dataFileName = string("libSVM.txt"));

void CompileDataFile(const string dataFile, Config_Module &tcon, int trainOrTest);

#endif