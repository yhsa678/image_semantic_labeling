#include "Util_Directory_File.h"

string FullFile(string f1, const string f2, int option)
{
	if (option == 1) return f1.append(f2);
	if (f1[f1.length()-1] != '\\') f1.append("\\");
	return f1.append(f2);
}

void CatTwoFiles(const string &f1, const string &f2)
{
	std::ofstream of_f1(f1, std::ios_base::binary | std::ios_base::app);
	std::ifstream if_f2(f2, std::ios_base::binary);

	of_f1.seekp(0, std::ios_base::end);
	of_f1 << if_f2.rdbuf();

	of_f1.close();
	if_f2.close();
}
