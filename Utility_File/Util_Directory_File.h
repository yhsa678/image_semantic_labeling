#ifndef __UTILITY_FILE_HEADER
#define __UTILITY_FILE_HEADER

#include <boost/filesystem.hpp>
using namespace boost::filesystem;

#ifdef WIN32
#include <direct.h>
#endif

#include <string>
using std::string;

string FullFile(string f1, const string f2, int option = 0);
void CatTwoFiles(const string &f1, const string &f2);

inline void MakeDeepDir(string &folderName)
{
	int found = folderName.find_first_of('\\');
	while (found != string::npos)
	{
		mkdir(folderName.substr(0, found).c_str());
		found = folderName.find_first_of('\\', found+1);
	}
}


inline bool ExistFile2(const std::string &name)
{
	//struct stat buffer;   
	//return (stat(name.c_str(), &buffer) == 0); 
	return false;
}

inline bool ExistFile(const std::string &name)
{
	if (FILE *file = fopen(name.c_str(), "r"))
	{
		fclose(file);
		return true;
	} 
	else 
	{
		return false;
	}   
}


#endif