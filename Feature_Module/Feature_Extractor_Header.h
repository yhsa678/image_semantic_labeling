#ifndef __FEATURE_EXTRACTOR_HEADER
#define __FEATURE_EXTRACTOR_HEADER

#include "opencv2/opencv.hpp"
#include <string>
using std::string;
#include <vector>
using std::vector;

#include "../Utility_File/Util_Directory_File.h"

#define MIN(x, y) (x) < (y)? (x): (y);
#define MAX(x, y) (x) > (y)? (x): (y);

class Feature_Extractor
{
public:
	Feature_Extractor(){};
	~Feature_Extractor(){};
	
	cv::Mat_<cv::Vec3b> localImg;
	cv::Mat_<int> localSeg;
	int numOfLocalSeg;
	vector<vector<cv::Vec2i>> segPixelPos;
	vector<vector<cv::Vec3b>> segPixelColor;

	string featureStoreFolder;
	int SetDataForExtractingFeature(const cv::Mat_<cv::Vec3b> &img, const cv::Mat_<int> &seg, int numOfSeg, string storeFolder);
	int ExtractFeatures(const vector<string> &featureList);

	vector<cv::Mat_<int>> labelColorMap;
	int uniqueLabelNumber;
	int InitLabelColorMapping(vector<vector<cv::Vec3b>> &labelColor);
	int ExtractLabel(const cv::Mat_<cv::Vec3b> &labelImg, const string labelStoreFolder, const string labelFile);

	void WriteOutColorFeature(const string featureFile);
	void WriteOutShapeSizeFeature(const string featureFile);
	void WriteOutLocationFeature(const string featureFile);
	void WriteOutTextureFeature(const string featureFile);
	void WriteOutAppearanceFeature(const string featureFile);
};

#endif