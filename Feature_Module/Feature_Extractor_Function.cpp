#include "Feature_Extractor_Header.h"

inline float Square(float val)
{
	return val*val;
}

inline float Rgb2Gray(float r, float g, float b)
{
	return r*0.299+g*0.587+b*0.114;
}

int Feature_Extractor::ExtractFeatures(const vector<string> &featureList )
{
	int iy;
	for (iy=0; iy<featureList.size(); ++iy)
	{
		if (featureList[iy] == "color")
		{
			WriteOutColorFeature(FullFile(featureStoreFolder, "color.txt"));
		}
		else if (featureList[iy] == "spshape")
		{
			WriteOutShapeSizeFeature(FullFile(featureStoreFolder, "spshape.txt"));
		}
		else if (featureList[iy] == "appearance")
		{
			WriteOutAppearanceFeature(FullFile(featureStoreFolder, "appearance.txt"));
		}
		else if (featureList[iy] == "location")
		{
			WriteOutLocationFeature(FullFile(featureStoreFolder, "location.txt"));
		}
		else if (featureList[iy] == "texture")
		{
			WriteOutTextureFeature(FullFile(featureStoreFolder, "texture.txt"));
		}
		
	}
	return 0;
}

int Feature_Extractor::SetDataForExtractingFeature( const cv::Mat_<cv::Vec3b> &img, const cv::Mat_<int> &seg, int numOfSeg, const string storeFolder )
{
	localImg = img.clone();
	localSeg = seg.clone();
	numOfLocalSeg = numOfSeg;
	featureStoreFolder = storeFolder;

	segPixelColor.clear();
	segPixelColor.resize(numOfLocalSeg);
	segPixelPos.clear();
	segPixelPos.resize(numOfLocalSeg);

	int height, width;
	int iy, ix;
	height = localImg.rows;
	width = localImg.cols;
	for (iy=0; iy<height; ++iy)
	{
		for (ix=0; ix<width; ++ix)
		{
			int segId = localSeg[iy][ix];
			segPixelPos[segId].push_back(cv::Vec2i(iy, ix));
			segPixelColor[segId].push_back(localImg[iy][ix]);
		}
	}
	return 0;
}

void Feature_Extractor::WriteOutColorFeature(const string featureFile)
{
	// 1*2: intensity mean and std dev
	// 3*2: rgb color mean and std dev
	// 8*3: rgb color histogram, normalized
	const int BIN_NUMBER = 8;
	const float BIN_GAP = 256.0/BIN_NUMBER;

	FILE *fout = fopen(featureFile.c_str(), "w");

	fprintf(fout, "%d %d\n", numOfLocalSeg, (1*2+3*2+3*BIN_NUMBER));

	int iy, ix;
	for (iy=0; iy<numOfLocalSeg; ++iy)
	{
		float intensityMean, intensitySquareMean;
		intensityMean = 0.0, intensitySquareMean = 0.0;
		cv::Vec3f colorMean;
		cv::Vec3f colorSquareMean;

		cv::Mat_<float> hist;
		hist.create(3, BIN_NUMBER);
		hist.setTo(0.0);

		colorMean = cv::Vec3f(0.0, 0.0, 0.0);
		colorSquareMean = cv::Vec3f(0.0, 0.0, 0.0);
		int num = segPixelColor[iy].size();
		for (ix=0; ix<num; ++ix)
		{
			cv::Vec3f tmpColor = segPixelColor[iy][ix];

			intensityMean += Rgb2Gray(tmpColor[2], tmpColor[1], tmpColor[0]);
			intensitySquareMean += Square(Rgb2Gray(tmpColor[2], tmpColor[1], tmpColor[0]));

			colorMean += tmpColor;
			colorSquareMean[0] += tmpColor[0]*tmpColor[0];
			colorSquareMean[1] += tmpColor[1]*tmpColor[1];
			colorSquareMean[2] += tmpColor[2]*tmpColor[2];

			hist[0][int(floor(tmpColor[0]/BIN_GAP))] += 1.0;
			hist[1][int(floor(tmpColor[1]/BIN_GAP))] += 1.0;
			hist[2][int(floor(tmpColor[2]/BIN_GAP))] += 1.0;
		}

		intensityMean /= num;
		intensitySquareMean /= num;
		fprintf(fout, "%f %f ", intensityMean, sqrt(intensitySquareMean-Square(intensityMean)));

		colorMean /= num;
		colorSquareMean /= num;
		fprintf(fout, "%f %f %f ", colorMean[0], colorMean[1], colorMean[2]);
		fprintf(fout, "%f %f %f ", sqrt(colorSquareMean[0]-Square(colorMean[0])), sqrt(colorSquareMean[1]-Square(colorMean[1])), sqrt(colorSquareMean[2]-Square(colorMean[2])));

		for (ix=0; ix<BIN_NUMBER; ++ix)
		{
			fprintf(fout, "%f %f %f ", hist[0][ix]/num, hist[1][ix]/num, hist[2][ix]/num);
		}

		fprintf(fout, "\n");
	}
	fclose(fout);
}

void Feature_Extractor::WriteOutShapeSizeFeature(const string featureFile )
{
	// 1: superpixel size/image size
	// 1: bounding box size/image size -- to be determined because its just the product of below two
	// 2: bounding box width, height/ image width, height
	// 8*8: superpixel shape mask over bounding box
	const int SPATIAL_BIN = 8;
	FILE *fout = fopen(featureFile.c_str(), "w");

	fprintf(fout, "%d %d\n", numOfLocalSeg, (1+1+2+SPATIAL_BIN*SPATIAL_BIN));

	int height, width;
	height = localImg.rows; width = localImg.cols;
	int iy, ix;
	for (iy=0; iy<numOfLocalSeg; ++iy)
	{
		int top, down, left, right;
		top = height, down = 0, left = width, right = 0;
		int num = segPixelPos[iy].size();
		for (ix=0; ix<num; ++ix)
		{
			cv::Vec2i tmpPos = segPixelPos[iy][ix];
			top = MIN(top, tmpPos[0]);
			down = MAX(down, tmpPos[0]);
			left = MIN(left, tmpPos[1]);
			right = MAX(right, tmpPos[1]);
		}

		float yGap = (down+1-top)/float(SPATIAL_BIN);
		float xGap = (right+1-left)/float(SPATIAL_BIN);

		cv::Mat_<float> spShape(SPATIAL_BIN, SPATIAL_BIN);
		spShape.setTo(0.0);
		for (ix=0; ix<num; ++ix)
		{
			cv::Vec2i tmpPos = segPixelPos[iy][ix];
			spShape[int(floor((tmpPos[0]-top)/yGap))][int(floor((tmpPos[1]-left)/xGap))] += 1.0;
		}

		fprintf(fout, "%f %f %f %f ", float(num)/(height*width), 
			float((down-top)*(right-left))/(height*width), 
			float(down-top)/height,
			float(right-left)/width);

		int k1, k2;
		for (k1=0; k1<SPATIAL_BIN; ++k1)
		{
			for (k2=0; k2<SPATIAL_BIN; ++k2)
			{
				// output only shape or distribution?
				//fprintf(fout, "%f ", spShape[k1][k2]>0.0? 1.0: 0.0);
				fprintf(fout, "%f ", spShape[k1][k2]/num);
			}
		}

		fprintf(fout, "\n");
	}
	fclose(fout);
}

void Feature_Extractor::WriteOutLocationFeature(const string featureFile )
{

}

void Feature_Extractor::WriteOutTextureFeature(const string featureFile )
{
	// texton, garbor filter, dense sift, dense daisy, bag of words, spatial pyramid
}

void Feature_Extractor::WriteOutAppearanceFeature(const string featureFile )
{
	// gist, bounding box thumbnail, superpixel (masked) thumbnail
}

int Feature_Extractor::InitLabelColorMapping( vector<vector<cv::Vec3b>> &labelColor )
{
	labelColorMap.clear();
	labelColorMap.resize(256);
	int iy, ix;
	for (iy=0; iy<256; ++iy)
	{
		labelColorMap[iy].create(256, 256);
		labelColorMap[iy].setTo(0);
	}

	for (iy=0; iy<labelColor.size(); ++iy)
	{
		for (ix=0; ix<labelColor[iy].size(); ++ix)
		{
			cv::Vec3b tmpC = labelColor[iy][ix];
			labelColorMap[tmpC[0]][tmpC[1]][tmpC[2]] = iy+1;
		}
	}

	uniqueLabelNumber = labelColor.size()+1;
	return 0;
}

int Feature_Extractor::ExtractLabel(const cv::Mat_<cv::Vec3b> &labelImg, string labelStoreFolder, string labelFile )
{
	FILE *fout = fopen(FullFile(labelStoreFolder, labelFile).c_str(), "w");

	fprintf(fout, "%d 1\n", numOfLocalSeg);

	int iy, ix;
	for (iy=0; iy<numOfLocalSeg; ++iy)
	{
		vector<int> cntSegLabel(uniqueLabelNumber, 0);
		for (ix=0; ix<segPixelPos[iy].size(); ++ix)
		{
			cv::Vec3b lc = labelImg[segPixelPos[iy][ix][0]][segPixelPos[iy][ix][1]];
			++cntSegLabel[labelColorMap[lc[0]][lc[1]][lc[2]]];
		}

		int maxId = 0;
		for (ix=0; ix<uniqueLabelNumber; ++ix)
		{
			if (cntSegLabel[ix] > cntSegLabel[maxId]) maxId = ix;
		}

		fprintf(fout, "%d\n", maxId);
	}

	fclose(fout);
	return 0;
}
