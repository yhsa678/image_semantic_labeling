#ifndef __SUPERPIXEL_MODULE_HEADER
#define __SUPERPIXEL_MODULE_HEADER
// A universe superpixel generating interface
// A few popular segmentation/superpixel would be added, including:
// 1) Efficient Graph-Based Image Segmentation - http://cs.brown.edu/~pff/segment/
// 2) SLIC
// 3) meanshift
// 4) cross-fusion
// 5) normailized cut
// 6) turbo superpixel?
//
// Dependency: OpenCV

#include "opencv2/core/core.hpp"
#include <vector>
using std::vector;
using std::string;

class Superpixel_Module
{
public:
	Superpixel_Module() {};
	~Superpixel_Module() {};
	int SetImage(const cv::Mat_<cv::Vec3b> &imgBGR);
	int SetParameters(vector<float> &para);
	int GetSuperpixels(cv::Mat_<int> &seg);
	int CreateDefaultParametersForMethod(int methodId, vector<float> &para);
	static int CreateSuperpixelsFromImage(const cv::Mat_<cv::Vec3b> &imgBGR, int methodId, cv::Mat_<int> &seg);
private:
	vector<float> localPara;
	cv::Mat_<cv::Vec3b> localImg;

public:
	// some utility functions to visualize, read and write segment file
	static void DrawContoursAroundSegments(const cv::Mat_<cv::Vec3b> &imgIn, const cv::Mat_<int> &segLabels, cv::Mat_<cv::Vec3b> &imgOut);
	static int WriteOutSegToFile(const cv::Mat_<int> &segLabels, int numOfSeg, const string filename);
	static int ReadInSegFromFile(const string filename, cv::Mat_<int> &segLabels, int &numOfSeg);
};




#endif