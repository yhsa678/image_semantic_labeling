#include "Superpixel_Header.h"

// For efficient graph-based segmentation
#include "Efficient_Graph_Based/image.h"
#include "Efficient_Graph_Based/misc.h"
#include "Efficient_Graph_Based/segment-image.h"

int Superpixel_Module::SetImage( const cv::Mat_<cv::Vec3b> &imgBGR )
{
	localImg = imgBGR.clone();
	assert(!localImg.empty());
	return 0;
}

int Superpixel_Module::SetParameters( vector<float> &para )
{
	localPara.assign(para.begin(), para.end());
	assert(!localPara.empty());
	return 0;
}

int Superpixel_Module::GetSuperpixels( cv::Mat_<int> &seg )
{
	int numOfSuperpixels = -1;
	if (int(localPara[0]) == 1)
	{
		// Efficient Graph-Based Image Segmentation
		float sigma = localPara[1];
		float k = localPara[2];
		int min_size = localPara[3];

		// convert to "image" format
		int width = localImg.cols;
		int height = localImg.rows;
		image<rgb> *input = new image<rgb>(width, height);
		int iy, ix;
		for (iy=0; iy<height; ++iy)
		{
			for (ix=0; ix<width; ++ix)
			{
				imRef(input, ix, iy).r = localImg[iy][ix][2];
				imRef(input, ix, iy).g = localImg[iy][ix][1];
				imRef(input, ix, iy).b = localImg[iy][ix][0];
			}
		}

		// do segmentation
		// and label each pixel after segmentation
		image<int> *segLabel = new image<int>(width, height, false);
		int num_ccs; 
		image<rgb> *segImg = segment_image(input, sigma, k, min_size, &num_ccs, segLabel); 
		
		seg.create(height, width);
		for (iy=0; iy<height; ++iy)
		{
			for (ix=0; ix<width; ++ix)
			{
				seg[iy][ix] = imRef(segLabel, ix, iy);
			}
		}

		input->~image();
		segImg->~image();
		segLabel->~image();

		numOfSuperpixels = num_ccs;
	}
	else if (int(localPara[0]) == 2)
	{

	}
	return numOfSuperpixels;
}

int Superpixel_Module::CreateDefaultParametersForMethod( int methodId, vector<float> &para )
{
	if (methodId == 1)
	{
		// usage: 1 sigma k min 
		// sigma: Used to smooth the input image before segmenting it.
		// k: Value for the threshold function.
		// min: Minimum component size enforced by post-processing.
		// Typical parameters are sigma = 0.5, k = 500, min = 20.
		para.resize(4);
		para[0] = 1;
		para[1] = 0.5;
		para[2] = 500;
		para[3] = 20;
	}
	else if (methodId == 2)
	{

	}
	return 0;
}

int Superpixel_Module::CreateSuperpixelsFromImage( const cv::Mat_<cv::Vec3b> &imgBGR, int methodId, cv::Mat_<int> &seg )
{
	Superpixel_Module sm;
	sm.SetImage(imgBGR);

	vector<float> tmpPara;
	sm.CreateDefaultParametersForMethod(methodId, tmpPara);
	sm.SetParameters(tmpPara);

	return (sm.GetSuperpixels(seg));
}

// utility functions
void Superpixel_Module::DrawContoursAroundSegments(const cv::Mat_<cv::Vec3b> &imgIn, const cv::Mat_<int> &segLabels, cv::Mat_<cv::Vec3b> &imgOut)
{
	const int dx8[8] = {-1, -1,  0,  1, 1, 1, 0, -1};
	const int dy8[8] = { 0, -1, -1, -1, 0, 1, 1,  1};

	int width = imgIn.cols;
	int height = imgIn.rows;

/*	int sz = width*height;

	vector<bool> istaken(sz, false);

	int mainindex(0);
	for( int j = 0; j < height; j++ )
	{
		for( int k = 0; k < width; k++ )
		{
			int np(0);
			for( int i = 0; i < 8; i++ )
			{
				int x = k + dx8[i];
				int y = j + dy8[i];

				if( (x >= 0 && x < width) && (y >= 0 && y < height) )
				{
					int index = y*width + x;

					if( false == istaken[index] )//comment this to obtain internal contours
					{
						if( labels[mainindex] != labels[index] ) np++;
					}
				}
			}
			if( np > 1 )//change to 2 or 3 for thinner lines
			{
				ubuff[mainindex] = color;
				istaken[mainindex] = true;
			}
			mainindex++;
		}
	}*/

	int *labels = (int *)segLabels.ptr(0);

	int sz = width*height;
	vector<bool> istaken(sz, false);
	vector<int> contourx(sz);
	vector<int> contoury(sz);
	int mainindex(0);
	int cind(0);
	for( int j = 0; j < height; j++ )
	{
		for( int k = 0; k < width; k++ )
		{
			int np(0);
			for( int i = 0; i < 8; i++ )
			{
				int x = k + dx8[i];
				int y = j + dy8[i];

				if( (x >= 0 && x < width) && (y >= 0 && y < height) )
				{
					int index = y*width + x;

					//if( false == istaken[index] )//comment this to obtain internal contours
					{
						if( labels[mainindex] != labels[index] ) np++;
					}
				}
			}
			if( np > 1 )
			{
				contourx[cind] = k;
				contoury[cind] = j;
				istaken[mainindex] = true;
				//img[mainindex] = color;
				cind++;
			}
			mainindex++;
		}
	}

	imgOut = imgIn.clone();

	int numboundpix = cind;//int(contourx.size());
	for( int j = 0; j < numboundpix; j++ )
	{
		int ii = contoury[j]*width + contourx[j];
		// ubuff[ii] = 0xffffff;
		imgOut[contoury[j]][contourx[j]] = cv::Vec3b(255, 255, 255);

		for( int n = 0; n < 8; n++ )
		{
			int x = contourx[j] + dx8[n];
			int y = contoury[j] + dy8[n];
			if( (x >= 0 && x < width) && (y >= 0 && y < height) )
			{
				int ind = y*width + x;
				// if(!istaken[ind]) ubuff[ind] = 0;
				if (!istaken[ind]) imgOut[y][x] = cv::Vec3b(0, 0, 0);
			}
		}
	}
}

int Superpixel_Module::WriteOutSegToFile( const cv::Mat_<int> &segLabels, int numOfSeg, const string filename )
{
	FILE *fout = fopen(filename.c_str(), "wb");
	assert(fout != NULL);

	fprintf(fout, "%d&%d&%d&", segLabels.rows, segLabels.cols, numOfSeg);
	fwrite(segLabels.data, sizeof(int), segLabels.rows*segLabels.cols, fout);

	fclose(fout);
	return 0;
}

int Superpixel_Module::ReadInSegFromFile( const string filename, cv::Mat_<int> &segLabels, int &numOfSeg )
{
	FILE *fin = fopen(filename.c_str(), "rb");
	assert(fin != NULL);

	int height, width, tmpNum;
	fscanf(fin, "%d&%d&%d&", &height, &width, &tmpNum);
	segLabels.create(height, width);
	numOfSeg = tmpNum;
	fread(segLabels.data, sizeof(int), height*width, fin);

	fclose(fin);
	return 0;
}