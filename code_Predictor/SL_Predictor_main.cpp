#include "../Config_Module/Config_Semantic_Labeling_Header.h"
#include "../Utility_File/Util_Directory_File.h"
#include "opencv2/opencv.hpp"

#include <iostream>
using namespace std;

#include "../Utility_File/Util_DataPreparation.h"
#include "../Utility_File/OpenCV_Tickcount_Header.h"

void TestOnTestDataFile(const string testDataFile, Config_Module &tcon);

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		printf("Lacking of task config file!\nPlease use relative path!\n");
		exit(-1);
	}

	char curDir[256];
	strcpy(curDir, argv[1]);
	while (curDir[strlen(curDir)-1] != '\\')
	{
		curDir[strlen(curDir)-1] = '\0';
	}
	printf("%s %d\n", curDir, chdir(curDir));
	int retVal = chdir(curDir);
	assert(0 == retVal);
	// read task configuration file
	string taskConfig(argv[1]);
	Config_Module tcon(taskConfig);
	const bool CLEAN_DATA_FLAG = tcon.CLEAN_EXIST_DATA_FLAG;

	printf("FINISH: read config file %s\n\n", argv[1]);
	char tmpCwd[256];
	getcwd(tmpCwd, 256);
	printf("Current working directory: %s\n\n", tmpCwd);


	int iy;
	for (iy=0; iy<tcon.testImgFileName.size(); ++iy)
	{
		string feaImgDir = FullFile(tcon.featureFolder, tcon.testImgFileFolderName[iy]);

		// clean output label file if needed
		if (CLEAN_DATA_FLAG) remove(FullFile(feaImgDir, tcon.labelOutFileSuffix));
		if (ExistFile(FullFile(feaImgDir, tcon.labelOutFileSuffix)))
		{
			cout << feaImgDir << " exist outputed label!! Skipped" << endl;
			continue;
		}
		//cout << feaImgDir << endl;

		string testDataFile = FullFile(feaImgDir, DATA_FILE_NAME);
		CalcTime ct;
		ct.Start();
		TestOnTestDataFile(testDataFile, tcon);
		ct.End("Each prediction of image costs");
		// printf("FINISH: test data file %s\n", testDataFile.c_str());
	}


	printf("START: reading and compile testing data\n\n");
	string testDataFile = FullFile(tcon.featureFolder, TESTING_DATA_FILE_NAME);
	
	CompileDataFile(testDataFile, tcon, 2);
	printf("FINISH: reading and compile testing data\n\n");;
	
	// test on compiled test data
	CalcTime ct;
	ct.Start();
	TestOnTestDataFile(testDataFile, tcon);
	ct.End("Total prediction of test images cost");
	printf("FINISH: test data file %s\n\n", testDataFile.c_str());
	
	return 0;
}

void TestOnTestDataFile(const string testDataFile, Config_Module &tcon)
{
	// test on test data
	string testDataScaled(FullFile(testDataFile, ".scale.txt", 1));
	char svmCmd[512];
	sprintf(svmCmd, "%s -r %s %s > %s\0", tcon.libSVMScaleExe.c_str(), tcon.libSVMScaledRange.c_str(), testDataFile.c_str(), testDataScaled.c_str());
	printf("%s\n", svmCmd);
	system(svmCmd);

	//if (tcon.libSVMTestPara.find())
	string testLabelOutput(FullFile(testDataFile, tcon.labelOutFileSuffix, 1));
	string testLabelAcc(FullFile(testDataFile, ".label.acc.txt", 1));
	sprintf(svmCmd, "%s %s %s %s %s > %s\0", tcon.libSVMPredictExe.c_str(), tcon.libSVMTestPara.c_str(), testDataScaled.c_str(), tcon.libSVMModelFile.c_str(), testLabelOutput.c_str(), testLabelAcc.c_str());
	printf("%s\n", svmCmd);
	system(svmCmd);

	printf("FINISH: output testing data label: %s\n\n", testLabelOutput.c_str());
}