# Image_Semantic_Labeling
A multi-class image semantic parsing project
 - Aim to provide pixel-wise semantic labeling for input images
 - Must be easy and efficient to use
 - Could serve as a building block to other computer vision tasks
 
*Started as a course project: [COMP 790-133: Recognizing People, Objects, and Actions](http://www.tamaraberg.com/teaching/Fall_13/)*

## Dependency library
 - OpenCV - http://opencv.org/
 - Boost::filesystem - http://www.boost.org/
